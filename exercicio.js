const alunos = [
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9
    }
]

// Resultado:
// O aluno Pedro teve a media mais alta da turma A, com 8.5 
// O aluno Maria teve a media mais alta da turma B, com 5.5
// O aluno Jonathan teve a media mais alta da turma C, com 9

for (let paulao = 0; paulao < alunos.length; paulao++) {
    const aluno = alunos[paulao];
    media = (aluno.nota1+aluno.nota2)/2
    console.log("O aluno "+ aluno.nome + " teve a media mais alta da turma" + aluno.turma +", com" + media)

    
}